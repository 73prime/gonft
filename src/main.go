package main

import (
	"errors"
	"fmt"
	"github.com/spf13/viper"
	"image"
	"image/draw"
	"image/png"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"
)

var (
	cfgFile, inputDir, outputDir, layers, rarities, keepaways string
	genNumber                                                 int
)

type FilenameStructure struct {
	Filename     string
	Rarity       string
	Layername    string
	FullFilename string
}
type StructureInventory struct {
	LayerNames  []string
	RarityNames []string
	Filenames   []FilenameStructure
}

func (si StructureInventory) GetLayerFileNames(layerName string) []FilenameStructure {
	result := []FilenameStructure{}
	for _, item := range si.Filenames {
		if item.Layername == layerName {
			result = append(result, item)
		}
	}
	return result
}

type NFT struct {
	Filename            string
	ShortLayerFileNames []string
	Rarity              string
}
type EditionsStructure struct {
	StructureInventory StructureInventory
	RarityLayout       map[string]float64
	NFTs               []NFT
}

var keepAway []NFT

func AreNFTsDifferent(nft1, nft2 NFT) bool {
	for idx1, _ := range nft1.ShortLayerFileNames {
		if nft1.ShortLayerFileNames[idx1] != nft2.ShortLayerFileNames[idx1] {
			return true
		}
	}
	return false
}

func (es *EditionsStructure) IsUnique(nft NFT) bool {
	if len(es.NFTs) == 0 {
		return true
	}
	for _, mint := range es.NFTs {
		if !AreNFTsDifferent(mint, nft) {
			return false
		}
	}
	return true

}

func (es *EditionsStructure) GetRandomLayer(layerName string) (image.Image, string) {
	layerFiles := es.StructureInventory.GetLayerFileNames(layerName)
	if len(layerFiles) > 0 {
		randomLayerIndex := rand.Intn(len(layerFiles))
		fmt.Println(layerFiles[randomLayerIndex].FullFilename)
		fImg, _ := os.Open(layerFiles[randomLayerIndex].FullFilename)
		defer fImg.Close()
		img, _, _ := image.Decode(fImg)
		return img, layerFiles[randomLayerIndex].Filename
	}
	return nil, ""
}

func (es *EditionsStructure) GenerateRandomImage(fileName string) bool {
	var images []image.Image
	var nft NFT
	if err := os.MkdirAll(outputDir, os.ModePerm); err != nil {
		log.Fatal(err)
	}
	nft.Filename = fileName
	for _, layer := range es.StructureInventory.LayerNames {
		img, layerFileName := es.GetRandomLayer(layer)
		nft.ShortLayerFileNames = append(nft.ShortLayerFileNames, layerFileName)
		images = append(images, img)
	}
	if !es.IsUnique(nft) {
		fmt.Println("A dupe!!")
		return false
	} else {
		for _, ka := range keepAway {
			if !AreNFTsDifferent(ka, nft) {
				fmt.Println("-------------------------already defined this as a keepaway==============================")
				return false
			}
		}
		es.NFTs = append(es.NFTs, nft)
	}
	if len(images) > 0 {
		width := images[0].Bounds().Size().X
		height := images[0].Bounds().Size().Y
		m := image.NewRGBA(image.Rect(0, 0, width, height))
		draw.Draw(m, m.Bounds(), images[0], image.Point{0, 0}, draw.Src)
		for idx, _ := range images {
			if idx > 0 {
				draw.Draw(m, m.Bounds(), images[idx], image.Point{0, 0}, draw.Over)
			}
		}
		toimg, _ := os.Create(fmt.Sprintf("%s/%s", outputDir, fileName))
		defer toimg.Close()
		fmt.Printf("Created file: %s\n", fileName)
		encoder := png.Encoder{
			CompressionLevel: png.BestCompression,
			BufferPool:       nil,
		}
		err := encoder.Encode(toimg, m)
		if err != nil {
			log.Fatal(err)
		}
	}
	return true
}
func ParseLayersRarities(layers string) ([]string, error) {
	results := strings.Split(layers, ",")
	if len(results) < 1 || results[0] == "" {
		return nil, errors.New("Your 'LAYERS' & 'RARITIES' envvars should have a comma delimited list of layers/rarities to process.")
	}
	return results, nil
}
func GenerateNFTs(es *EditionsStructure, outputdir string) {
	fmt.Println("Generating NFTs...")
	for i := 0; i < genNumber; i++ {
		fName := i + 1
		if !es.GenerateRandomImage(fmt.Sprintf("%d.png", fName)) {
			i--
		}
	}
}

func IsValid(names []string, folderName string) bool {
	for i := 0; i < len(names); i++ {
		if names[i] == folderName {
			return true
		}
	}
	return false
}
func GetLayerRarityFiles(inventory StructureInventory, layerName, rarityName string) []FilenameStructure {
	result := []FilenameStructure{}
	for _, file := range inventory.Filenames {
		if file.Layername == layerName && file.Rarity == rarityName {
			result = append(result, file)
		}
	}
	return result
}
func StageKeepAwayList() {
	keepaways := strings.Replace(keepaways, "\"", "", -1)
	keepawayEntries := strings.Split(keepaways, ";")
	for _, ke := range keepawayEntries {
		keepAwayLayers, err := ParseLayersRarities(ke)
		if err == nil {
			nft := NFT{}
			for _, kal := range keepAwayLayers {
				nft.ShortLayerFileNames = append(nft.ShortLayerFileNames, kal)
			}
			keepAway = append(keepAway, nft)
			log.Printf("Processed Keepaway entry, with %d layers.\n", len(nft.ShortLayerFileNames))
		}
	}

}
func Setup() (*EditionsStructure, error) {
	StageKeepAwayList()
	layerSet, err := ParseLayersRarities(layers)
	if err != nil {
		return nil, err
	}
	raritiesSet, err := ParseLayersRarities(rarities)
	if err != nil {
		return nil, err
	}
	Edition := &EditionsStructure{}
	imgStruct := StructureInventory{}
	for _, l := range layerSet {
		imgStruct.LayerNames = append(imgStruct.LayerNames, l)
	}
	for _, r := range raritiesSet {
		imgStruct.RarityNames = append(imgStruct.RarityNames, r)
	}

	layerDirs, err := ioutil.ReadDir(inputDir)
	if err != nil {
		log.Fatal(err)
	}
	for _, layerDir := range layerDirs {
		if layerDir.IsDir() && IsValid(imgStruct.LayerNames, layerDir.Name()) {
			rarityDirs, _ := ioutil.ReadDir(fmt.Sprintf("%s/%s", inputDir, layerDir.Name()))
			for _, rarityDir := range rarityDirs {
				if rarityDir.IsDir() && IsValid(imgStruct.RarityNames, rarityDir.Name()) {
					files, _ := ioutil.ReadDir(fmt.Sprintf("%s/%s/%s", inputDir, layerDir.Name(), rarityDir.Name()))
					for _, file := range files {
						fmt.Printf("%s\n", file.Name())
						fNameStruct := FilenameStructure{
							Filename:     file.Name(),
							Rarity:       rarityDir.Name(),
							Layername:    layerDir.Name(),
							FullFilename: fmt.Sprintf("%s/%s/%s/%s", inputDir, layerDir.Name(), rarityDir.Name(), file.Name()),
						}
						imgStruct.Filenames = append(imgStruct.Filenames, fNameStruct)
					}
				}
			}
		}
	}
	combinations := 0

	for _, layerName := range imgStruct.LayerNames {

		for _, rarityName := range imgStruct.RarityNames {
			files := GetLayerRarityFiles(imgStruct, layerName, rarityName)
			fmt.Printf("There are %d images that have the %s layer, %s rarity.\n", len(files), layerName, rarityName)
			combinations += len(files)
		}
	}
	fmt.Printf("There are %d images, that can produce %d combinations.\n", len(imgStruct.Filenames), 7)
	Edition.StructureInventory = imgStruct
	return Edition, nil
}
func InitConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		home, _ := os.UserHomeDir()
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName("gonft")
	}
	viper.AutomaticEnv() // read in environment variables that match
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
	CollectFlags()
}
func CollectFlags() {
	inputDir = viper.GetString("INPUT_DIR")
	outputDir = viper.GetString("OUTPUT_DIR")
	layers = viper.GetString("LAYERS")
	rarities = viper.GetString("RARITIES")
	if rarities == "" {
		rarities = "original,rare,super_rare"
	}
	genNumber = viper.GetInt("GEN_NUMBER")
	if genNumber < 1 {
		fmt.Println("GEN_NUMBER wasn't set, so defaulting to only 1 image to produce.")
		genNumber = 1
	}
	keepaways = viper.GetString("KEEP_AWAYS")
	if inputDir == "" || outputDir == "" || layers == "" {
		log.Panic("You must set INPUT_DIR, OUTPUT_DIR, LAYERS somewhere, either flags, envvars, or in the config file named gonft.yaml")
	}
}

func main() {
	fmt.Println("Go Program started....")
	InitConfig()
	rand.Seed(time.Now().UnixNano())
	editions, err := Setup()
	if err != nil {
		log.Fatal(err)
	}
	GenerateNFTs(editions, outputDir)

}
