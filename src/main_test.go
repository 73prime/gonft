package main

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
	"path"
	"runtime"
	"testing"
)

func setupMain() {
}
func teardownMain() {
}
func TestMain(m *testing.M) {
	setupMain()
	code := m.Run()
	teardownMain()
	os.Exit(code)
}

func TestGenerateNFTs(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	currdir := path.Dir(filename)
	fixturedir := currdir + "/../fixtures"
	t.Logf("Fixture directory: %s", fixturedir)

	os.Setenv("INPUT_DIR", fmt.Sprintf("%s/input", fixturedir))
	os.Setenv("OUTPUT_DIR", fmt.Sprintf("%s/output", fixturedir))
	os.Setenv("LAYERS", "layer1")
	InitConfig()
	es, err := Setup()
	if err != nil {
		t.Errorf("Threw an error: %s", err.Error())
	}
	GenerateNFTs(es, os.Getenv("OUTPUT_DIR"))
}
func TestSetup(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	currdir := path.Dir(filename)
	fixturedir := currdir + "/../fixtures"
	t.Logf("Fixture directory: %s", fixturedir)

	os.Setenv("INPUT_DIR", fmt.Sprintf("%s/input", fixturedir))
	os.Setenv("OUTPUT_DIR", fmt.Sprintf("%s/output", fixturedir))
	os.Setenv("LAYERS", "layer1")
	InitConfig()
	es, err := Setup()
	if err != nil {
		t.Errorf("Threw an error: %s", err.Error())
	}
	expected := 1
	actual := len(es.StructureInventory.LayerNames)
	if actual != expected {
		t.Errorf("Expected %d, got %d", expected, actual)
	}

}
func TestInitConfig(t *testing.T) {
	expectedIn := "input"
	expectedOut := "output"
	expectedLayers := "blah,blah2"
	os.Setenv("INPUT_DIR", expectedIn)
	os.Setenv("OUTPUT_DIR", expectedOut)
	os.Setenv("LAYERS", expectedLayers)
	InitConfig()
	if inputDir != expectedIn {
		t.Errorf("Expected %s, got %s", expectedIn, inputDir)
	}

}
func TestStageKeepAwayList(t *testing.T) {
	keepawaystr := "transparent.png,circle_white.png,base-logo.png,blue.png,blue.png,transparent.png,transparent.png"
	os.Setenv("KEEP_AWAYS", keepawaystr)
	viper.AutomaticEnv()
	keepaways = viper.GetString("KEEP_AWAYS")
	if keepaways == "" {
		t.Errorf("KEEP_AWAYS should have been set to read.")
	}
	StageKeepAwayList()
	if len(keepAway) == 0 {
		t.Errorf("Expected to have at least one keepaway entry.")
	}
	expected := "base-logo.png"
	actual := keepAway[0].ShortLayerFileNames[2]
	if actual != expected {
		t.Errorf("Expected %s, got %s", expected, actual)
	}
	keepAway = nil
	keepawaystr = "transparent.png,circle_white.png;base-logo.png,blue.png"
	os.Setenv("KEEP_AWAYS", keepawaystr)
	viper.AutomaticEnv()
	keepaways = viper.GetString("KEEP_AWAYS")
	if keepaways == "" {
		t.Errorf("KEEP_AWAYS should have been set to read.")
	}
	StageKeepAwayList()
	if len(keepAway) == 0 {
		t.Errorf("Expected to have at least one keepaway entry.")
	}
	expected = "base-logo.png"
	actual = keepAway[1].ShortLayerFileNames[0]
	if actual != expected {
		t.Errorf("Expected %s, got %s", expected, actual)
	}
}
func TestParseLayers(t *testing.T) {
	test := "layer1,layer2,"
	expected := "layer2"
	parts, err := ParseLayersRarities(test)
	if err != nil {
		t.Errorf("Threw an error: %s", err.Error())
	}
	actual := parts[1]
	if expected != actual {
		t.Errorf("Expected %s, got %s", expected, actual)
	}

	test = "layer1"
	expected = "layer1"
	parts, err = ParseLayersRarities(test)
	if err != nil {
		t.Errorf("Threw an error: %s", err.Error())
	}
	actual = parts[0]
	if expected != actual {
		t.Errorf("Expected %s, got %s", expected, actual)
	}

	test = ""
	expected = ""
	parts, err = ParseLayersRarities(test)
	if err == nil || parts != nil {
		t.Errorf("Should have thrown an error")
	}
}
