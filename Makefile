.PHONY: default
default: displayhelp ;

displayhelp:
	@echo Use "clean, showcoverage, tests, build, docker or run" with make, por favor.

showcoverage: tests
	@echo Running Coverage output
	go tool cover -html=coverage.out

tests: clean
	@echo Running Tests
	go test --coverprofile=coverage.out ./...

run: build
	@echo Running program
	./bin/gonft

build: clean
	@echo Running build command
	go build -o bin/gonft src/main.go

clean:
	@echo Removing binary TODO
	rm -rf ./bin ./vendor Gopkg.lock

docker:
	docker build -t gonft:latest . -f Dockerfile
	docker run -v $(PWD):/app/ -e INPUT_DIR=/app/fixtures/input -e OUTPUT_DIR=/app/fixtures/output -e LAYERS=layer1 -it gonft:latest go run src/main.go

