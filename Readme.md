## What is this?

First off, this is rough.

This is a Go program that will auto-generate PNG's based on layers for the purposes of creating NFT collections.  There are many missing features. :)

### How to use

* First, layout your directory with "input" subdir
```
.\src
.\input
.\input\layer1\original\blah.png
.\input\layer1\original\blah2.png
.\input\layer2\original\blah3.png
.\input\layer2\original\blah3.png
```
* Run locally:
```
INPUT_DIR=$(pwd)/input OUTPUT_DIR=$(pwd)/output LAYERS=layer1,layer2 GEN_NUMBER=10 make run      
```
* Create some adjustments with a YAML file or envvars. Valid Envvars are:
  * INPUT_DIR (required)...this is the root of your input
  * OUTPUT_DIR (required)...this is the root of your output
  * LAYERS (required)...this is the comma delimited layers for your generated images; will map to folder names
  * RARITIES (optional)...defaults to original,rare,super_rare....this is the subfolders. These will be used in a following release
  * GEN_NUMBER (optional)...defaults to 1, this is the number to generate.
  * KEEP_AWAYS (optional)...this is the layers that map to PNG's already generated. Comma delimited layers, semicolon delimited images:
```
KEEP_AWAYS="transparent.png,circle_blue;bg_blue.png,circle_light-blue.png"
```